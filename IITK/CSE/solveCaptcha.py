from keras.models import load_model
from imutils import paths
import numpy as np
import imutils
import cv2
import pickle
import sys


MODEL_FILENAME = "captcha_model.hdf5"
MODEL_LABELS_FILENAME = "model_labels.dat"

with open(MODEL_LABELS_FILENAME, "rb") as f:
    lb = pickle.load(f)

model = load_model(MODEL_FILENAME)

image_file=sys.argv[-1]

image = cv2.imread(image_file,0)
y=8
h=31
w=16
letter_image_regions = []
for x in range(19,99,w):
    crop_img = image[y:y+h, x:x+w]
    thresh = 90
    im_bw = cv2.threshold(crop_img, thresh, 255, cv2.THRESH_BINARY)[1]
    letter_image_regions.append(im_bw)

predictions = []
for letter_image in letter_image_regions:
    letter_image = np.expand_dims(letter_image, axis=2)
    letter_image = np.expand_dims(letter_image, axis=0)
    prediction = model.predict(letter_image)
    letter = lb.inverse_transform(prediction)[0]
    predictions.append(letter)

captcha_text = "".join(predictions)
print(captcha_text)
